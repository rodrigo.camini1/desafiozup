import { When, Then } from 'cucumber';
import { confLoginWCDSuccessEmail, confLoginWCDSuccessPassword } from '../../../constants';
import { loginPage } from '../../../../../../Users/Rodrigo Camini/Documents/desafiozup/src/pages/loginPage'
import { expect } from 'chai';

Given('the user login is done', function () {
    loginPage.loginSucess();
});

When('the user select the login menu Amazon', function () {
    loginPage.selectMenuLogin();
});

When('he user insert his email', function() {
    loginPage.loginSucess(confLoginWCDSuccessEmail, confLoginWCDSuccessPassword);
});

Then('the login title should be displayed', function () {
    expect(browser.getTitle()).to.contain("Sua Amazon.com.br");
});

