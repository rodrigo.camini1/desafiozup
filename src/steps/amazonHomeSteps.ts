import { When, Then } from 'cucumber';
import { amazonPage } from '../../../../../../Users/Rodrigo Camini/Documents/desafiozup/src/pages/amazonPage'
import { expect } from 'chai';


Given('the user search for a any product', function(productSearch) {
    amazonPage.searchOffer(productSearch);
    amazonPage.clickSearchOfferButton();
});

Then('the user choose a product from list', function() {
    amazonPage.selectItemInTheList();
});

Then('add product at shopping cart', function() {
    amazonPage.selectCartPorduct();
});

Then('the item should be displayed at shopping cart', function() {
    expect(amazonPage.checkProduct()).to.be.true;
});





