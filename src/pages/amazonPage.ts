import { utils } from "C:\Users\Rodrigo Camini\Documents\desafiozup\support\utils.ts";

export default class amazonPage{

    searchOfferInput: string = 'id=twotabsearchtextbox';
    searchOfferButton: string = '//input[@value="Ir"]';
    CartProductButton: string = 'id=add-to-cart-button';
    selectProductList:string = '//img[@alt="${product}\"]';
    searchList: string = '.car-list-container .arrow-right';

    navCarButton:string = 'id=nav-cart-one'
    checkCart:string = 'id=nav-cart-one'

    searchOffer(offerSearched){
        browser.waitForVisible(this.searchOfferInput);
        utils.setValueSafe(this.searchOfferInput, offerSearched)
        this.clickSearchOfferButton();
    }

    clickSearchOfferButton(){
        browser.waitForVisible(this.searchOfferButton);
        browser.element(this.searchOfferButton).click();        
    }

    checkCartProductVisible(){
        browser.waitForVisible(this.checkCart);
        return browser.element(this.checkCart).isVisible();
    }

    selectCartPorduct()
    {
        browser.waitForVisible(this.CartProductButton);
        browser.element(this.CartProductButton).click(); 
    }

    selectItemInTheList(index) {
        browser.waitForVisible(this.selectProductList);
        browser.element(this.selectProductList.replace('${product}', index)).click();
    }

} 