
export default class loginPage{

    private menuLogin: string = '//div[2]/div[2]/div/div/div/a/span';
    private loginButton: string = 'id=ap-credential-autofill-hint';
    private emailField: string = 'id=ap_email';
    private continueButton: string = '//input[@id="continue"]';
    private passwordField: string = 'id=ap_password';

    selectMenuLogin(): void {
        browser.element(this.menuLogin).click();
    }
   
    setEmailField(name: string){
        browser.waitForVisible(this.emailField);
        browser.element(this.emailField).setValue(name);
    }
 
    selectContinueButton(){
        browser.element(this.continueButton).click();
    }

    setPasswordField(name: string)
    {
        browser.waitForVisible(this.passwordField);
        browser.element(this.passwordField).setValue(name);;
    }

    getLogiTitle(loginHome: string){
        browser.getTitle();
        return browser.getTitle();
    }

    loginSucess(user, password){
        browser.waitForVisible(this.menuLogin);
        browser.element(this.menuLogin).click();
        browser.waitForVisible(this.emailField);
        browser.element(this.emailField).setValue(user);
        browser.waitForVisible(this.continueButton).click();
        browser.waitForVisible(this.passwordField);
        browser.element(this.passwordField).setValue(password);
        browser.element(this.loginButton).click();
    }
    
} 