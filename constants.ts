import AmazonPage from "/pages/amazonPage";
import loginPage from "/pages/loginPage";
import Config from "/support";

export const criteria: string = browser.desiredCapabilities.browserName;
export const amazonPage: AmazonPage = new AmazonPage(criteria);
export const confLoginEmail: string = Config.get("user.loginSuccess.email");
export const confPassword: string = Config.get('user.loginSuccess.password');
