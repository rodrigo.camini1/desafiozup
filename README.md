DESAFIO ZUP
Este projeto foi criado com o intuito de participar do desafio de automação ZUP, o mesmo foi desenvolvido em TypeScripit com Cucumber#.

Site utilizado amazon.com.br

Metas deste projeto:

1- Entre em um grande portal de comércio online
(Exemplo: Americanas, Submarino, Magazine Luiza)
2- Faça uma busca por um produto
3- Valide o retorno da busca
4- Escolha um produto na lista
5- Adicione o carrinho
6- Valide o produto no carrinho

Para a criação dos cenários de testes devem ser considerados casos de sucesso e fluxos
alternativos
Sugestões de ferramentas para automação: Selenium,Junit, Testng, cucumber e
linguagens como: Ruby, Java, Python, Javascript. Ao finalizar o levantamento dos cenários
e a automação, posteno github ou bitbucket o código e cenários. Não se esqueça de deixar
público e nos enviar o link.

Estrutura de pastas

O projeto é dividido na seguinte lógica de estrutura de pastas:

/src/features - Onde estão localizados os cenários de teste no padrão BDD.

/src/steps - Onde a implementação dos passos definidos nas features deve ser criada.

/serc/pages Onde a inicialização das classes dos componentes da pagina amazon deve ser realizada
Utilizado para testes e2e.

Instalando dependências

Para instalar as dependências do projeto que estão especificadas no arquivo package.json, o seguinte comando deve ser executado na pasta
raiz do projeto:

$ npm install

As dependências instaladas ficarão disponíveis em uma pasta denominada "node_modules".

O arquivo wdio

O arquivo wdio.conf.js é o arquivo de configuração do projeto que contém todas as informações necessárias para executar sua suíte de testes. Este arquivo foi utilizado no projeto como base para todas as configurações do projeto como: features a serem
executadas, configuração de timeout, screenshot, log do resultado dos testes, configurações do Cucumber, entre outras. Por default, este
arquivo possui a configuração de executar os testes em um ambiente local.

Executando os testes

Como o Typescript é a linguagem utilizada no projeto, é importante saber que os arquivos .ts devem ser compilados para arquivos .js. Portanto,
para qualquer novo arquivo .ts ou qualquer alteração executada nos arquivos já existentes, a compilação deve ser realizada.

Para compilar os arquivos .ts, no diretório raiz do projeto, o seguinte comando deve ser executado:

$ npm run test:run

O arquivo package.json do projeto contém alguns scripts prontos dos comandos de execução dos testes. Portanto, após compilar os arquivos,
para executar os testes, execute o seguinte comando:

Execução em ambiente local:
$ npm run test:local
