var defaults = require('./wdio.conf.js').config;
var _ = require('lodash');
var utils = require('./utils').utils;
var chromeOptionsArgs = [ '--headless', '--disable-gpu', '--no-sandbox', '--disable-dev-shm-usage', '--window-size=1366,768'];

var overrides = {

    capabilities: [
        {
            browserName: 'chrome_desktop',
            chromeOptions: {
                'args': chromeOptionsArgs
            },
            specs: [
                './src/feature/*.feature',
            ],
            exclude: [
                
            ]
        },
        // {
        //     browserName: 'chrome_mobile',
        //     chromeOptions: {
        //         args: chromeOptionsArgs,
        //         mobileEmulation: {
        //             deviceName: 'Pixel 2'
        //         }
        //     },
        //     specs: [
        //         './src/feature/*.feature'
        //     ],
        //     exclude: [
        //     ]
        // },
        // 
        //
    ],
    baseUrl: 'https://www.amazon.com.br/',

};
defaults.capabilities = overrides.capabilities.map(capability => {
    return utils.getDeviceReport(capability);
});

// Send the merged config to wdio
exports.config = _.defaultsDeep(overrides, defaults);
