exports.utils = {
    getDeviceReport(capability){
            return {
                browserName: 'chrome',
                metadata: {
                    device:  capability.chromeOptions.mobileEmulation ? capability.chromeOptions.mobileEmulation.deviceName : 'Desktop',
                    platform: {
                        name: capability.chromeOptions.mobileEmulation? 'android' : 'osx'
                    }
                }
            }
         }
    }