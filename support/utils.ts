import currency from 'currency.js'

export default class Utils {

    scrollToEndOfPage(elementScroll) {
        browser.execute(
            function (elementScroll) {
                document.querySelector(elementScroll).scrollTo(0, document.querySelector(elementScroll).scrollHeight);
            }, elementScroll)
    }

    clickButtonJquery(clickElement){
        browser.execute(
            function (clickElement) {
                document.querySelector(clickElement).click();
            }, clickElement
        )
    }

    setValueSafe(selector: string, text: string){
        let
        getActualText = elementId =>
          browser
            .elementIdAttribute(elementId, 'value')
            .value
        ,
        setChar = (elementId, text, i) => {
          const
            currentChar  = text[i],
            expectedText = text.slice(0, i + 1);

      // Send keystroke.
          browser.elementIdValue(elementId, currentChar);

      // Wait for text to be actually entered.
          browser.waitUntil(() => getActualText(elementId) == expectedText, 1000, "failed", 16);
        };

      // Get the ID of the selected elements WebElement JSON object.
      const elementId = browser.element(selector).value.ELEMENT;

      // Clear the input before entering new value.
      browser.elementIdClear(elementId);
      browser.waitUntil(() => getActualText(elementId) == '');

      // Set each character of the text separately with setChar().
      for (let i = 0; i < text.length; i++) {
        setChar(elementId, text, i);
      }
}

    waitTextContainInElement(element, text)
    {
        browser.waitUntil(() => {
            return browser.element(element).getText().toUpperCase().includes(text.toUpperCase());
        });
    }
}


